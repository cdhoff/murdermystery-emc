package com.cdhoff.murdermystery.Client.Modules;

import com.cdhoff.murdermystery.Client.Client;
import com.cdhoff.murdermystery.Client.Modules.Mods.*;
import com.cdhoff.murdermystery.Client.Utils.KeybindManager;
import me.deftware.client.framework.chat.ChatBuilder;
import me.deftware.client.framework.chat.style.ChatColors;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.*;

import java.util.Map;
import java.util.TreeMap;

public class ModLoader {

    private Map<ModType, Mod> modlist = new TreeMap<>();

    public void initialize() {
        //modlist.put(ModType.MURDERMYSTERYAUTOSEND, new MurdererAutoSend());
        //modlist.put(ModType.OBSIDIANFINDER, new ObsidianFinder());
        modlist.put(ModType.MURDERMYSTERY, new MurderMystery());
        modlist.put(ModType.FULLBRIGHT, new Fullbright());
        modlist.put(ModType.TRUESIGHT, new TrueSight());
        //modlist.put(ModType.AUTOTOOL, new AutoTool());
        modlist.put(ModType.NAMETAGS, new Nametags());
        modlist.put(ModType.WALLHACK, new PlayerGlow());
        modlist.put(ModType.TRACERS, new Tracers());
        modlist.put(ModType.PLAYERESP, new PlayerESP());
        modlist.put(ModType.WAILA, new WAILA());
        modlist.put(ModType.ITEMESP, new ItemESP());
        modlist.put(ModType.HOSTILEMOBESP, new HostileMobESP());
        modlist.put(ModType.PASSIVEMOBESP, new PassiveMobESP());
        modlist.put(ModType.PASSIVEGLOW, new PassiveGlow());
        modlist.put(ModType.HOSTILEGLOW, new HostileGlow());
        modlist.put(ModType.ITEMGLOW, new ItemGlow());

    }

    public Map<ModType, Mod> getModlist() {
        return modlist;
    }

    public Mod getMod(ModType mod) {
        return modlist.get(mod);
    }

    public Mod getMod(String mod) {
        for (Mod m : modlist.values()) {
            if (m.getName().toLowerCase().equals(mod.toLowerCase())) {
                return m;
            }
        }
        return null;
    }

    public void handleToggleEvent(Event event) {

        EventKeyAction keypress = (EventKeyAction) event;

        if(!KeybindManager.bind) {
            for (Mod m : modlist.values()) {

                if (m.getKeybind() == keypress.getKeyCode()) {
                    m.toggle();
                }
            }
        }else {
            if (((EventKeyAction) event).getKeyCode() != 47) {
                getMod(KeybindManager.latestModname).setKeybind(((EventKeyAction) event).getKeyCode());
                new ChatBuilder().withText(KeybindManager.latestModname.toUpperCase() + " has been set to keycode " + ((EventKeyAction) event).getKeyCode()).withColor(ChatColors.WHITE).build().print();
                KeybindManager.bind = false;
                KeybindManager.latestModname = "dicks";
            }
        }
    }

    public void handleUpdateEvent(EventUpdate event) {

        for(Mod m : Client.getInstance().getModloader().getModlist().values()) {
            if (m.isState()) {
                m.onEvent(event);
            }
        }

    }

    public void handleDrawEvent(EventRenderHotbar event) {
        for(Mod m : Client.getInstance().getModloader().getModlist().values()) {
            if (m.isState()) {
                m.onDraw(event);
            }
        }
    }

    public void handleWorldEvent(EventRender3D event) {
        for(Mod m : Client.getInstance().getModloader().getModlist().values()) {
            if (m.isState()) {
                m.onWorld(event);
            }
        }
    }

    public void handleNametag(EventNametagRender event) {
        for(Mod m : Client.getInstance().getModloader().getModlist().values()) {
            if (m.isState()) {
                m.onNametag(event);
            }
        }
    }
}
