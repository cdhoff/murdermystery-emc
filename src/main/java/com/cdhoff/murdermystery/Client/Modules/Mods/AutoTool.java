package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Client;
import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.entity.IPlayer;
import me.deftware.client.framework.wrappers.item.*;
import me.deftware.client.framework.wrappers.world.IBlockPos;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

public class AutoTool extends Mod {

    public AutoTool() {
        super("AutoTool", "", ModType.AUTOTOOL, GLFW.GLFW_KEY_UP);
    }

    public static int action = 0;

    @Override
    public void onEvent(Event event) {
        if(action == 1){
            equipBestTool(IMinecraft.getBlockOver());
        }
    }

    public static void equipBestTool(IBlockPos pos)
    {
        IPlayer player = Client.getSelf();
        if(player.isCreative())
            return;

//        class_2680 state = IWorld.getStateFromPos(pos);

        IItemStack heldItem = player.getHeldItem();
        float bestSpeed = getDestroySpeed(heldItem, pos);
        int bestSlot = -1;

        int fallbackSlot = -1;

        for(int slot = 0; slot < 9; slot++)
        {
            if(slot == IInventory.getCurrentItem())
                continue;

            IItemStack stack = IInventoryWrapper.getStackInSlot(slot);

            if(fallbackSlot == -1 && !isDamageable(stack))
                fallbackSlot = slot;

            float speed = getDestroySpeed(stack, pos);
            if(speed <= bestSpeed)
                continue;

            if(isTooDamaged(stack))
                continue;

            bestSpeed = speed;
            bestSlot = slot;
        }

        boolean useFallback =
                isDamageable(heldItem) && (isTooDamaged(heldItem)
                        && getDestroySpeed(heldItem, pos) <= 1);

        if(bestSlot != -1)
        {
            IInventory.setCurrentItem(bestSlot);
            return;
        }

        if(!useFallback)
            return;

        if(fallbackSlot != -1)
        {
            IInventory.setCurrentItem(fallbackSlot);
            return;
        }

        if(isTooDamaged(heldItem))
            if(IInventory.getCurrentItem() == 8)
                IInventory.setCurrentItem(0);
            else
                IInventory.setCurrentItem(IInventory.getCurrentItem()+1);
    }

    private static float getDestroySpeed(IItemStack stack, IBlockPos pos)
    {
        float speed = (stack == null || stack.isEmpty()) ? 1 : stack.getStrVsBlock(pos);

        if(speed > 1)
        {
            int efficiency = stack
                    .getEnchantmentLevel(IEnchantment.getByName("EFFICIENCY"));
            if(efficiency > 0 && !(stack == null || stack.isEmpty()))
                speed += efficiency * efficiency + 1;
        }

        return speed;
    }

    private static boolean isDamageable(IItemStack stack)
    {
        return !(stack == null || stack.isEmpty());
    }

    private static boolean isTooDamaged(IItemStack stack)
    {
        return stack.getMaxDamage() - stack.getDamage() <= 4;
    }

    public void setSlot()
    {
        // find best weapon
        float bestValue = Integer.MIN_VALUE;
        int bestSlot = -1;
        for(int i = 0; i < 9; i++)
        {
            // skip empty slots
            if(IInventoryWrapper.getStackInSlot(i).isEmpty())
                continue;

            IItem item = IInventoryWrapper.getStackInSlot(i).getIItem();

            // get damage
            float value = getValue(item);

            // compare with previous best weapon
            if(value > bestValue)
            {
                bestValue = value;
                bestSlot = i;
            }
        }

        // check if any weapon was found
        if(bestSlot == -1)
            return;

        // save old slot
//        if(oldSlot == 0)
//            oldSlot = Minecraft.getMinecraft().player.inventory.currentItem;

        // set slot
        IInventory.setCurrentItem(bestSlot);

        // start timer
    }

    private float getValue(IItem item)
    {
        return item.getAttackDamage();
    }


    @Override
    public void onDraw(Event event) {

    }

    @Override
    public void onWorld(Event event) {

    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }
}
