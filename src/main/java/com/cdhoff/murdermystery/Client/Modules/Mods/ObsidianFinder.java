package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.entity.IEntityPlayer;
import me.deftware.client.framework.wrappers.world.IBlock;
import me.deftware.client.framework.wrappers.world.IBlockPos;
import me.deftware.client.framework.wrappers.world.IWorld;
//import net.minecraft.class_310;
//import net.minecraft.class_3532;
import net.minecraft.class_310;
import net.minecraft.class_3532;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;

public class ObsidianFinder extends Mod {

    public ObsidianFinder() {
        super("ObbyFinder","", ModType.OBSIDIANFINDER, GLFW.GLFW_KEY_HOME);
    }

    @Override
    public void onEvent(Event event) {

    }

    public static void updateBlocks(){

        obbyBlocks.clear();

        getLoadedObby(80,10,80).forEach(block -> {
            if (!obbyBlocks.contains(block)) {
                obbyBlocks.add(block);
            }
        });
    }

    @Override
    public void onDraw(Event event) {

    }

    int timer = 0;

    static ArrayList<IBlock> obbyBlocks = new ArrayList<IBlock>();

    @Override
    public void onWorld(Event event) {


        for(IBlock block : obbyBlocks){
            RenderUtils.ESPBox(block.getBlockPos(),0.13,0.11,0.18,0.6,0,false);
        }


    }

    public static ArrayList<IBlock> getLoadedObby(int xRange, int yRange, int zRange) {
        ArrayList<IBlock> blocks = new ArrayList();
        if (class_310.method_1551().field_1724 != null && class_310.method_1551().field_1687 != null) {
            for(int xRange1 = -xRange; xRange1 <= xRange; ++xRange1) {
                for(int yRange1 = -yRange; yRange1 <= yRange; ++yRange1) {
                    for(int zRange1 = -zRange; zRange1 <= zRange; ++zRange1) {
                        int posX = (int)(IEntityPlayer.getPosX() + (double)xRange1);
                        int posY = (int) class_3532.method_15350(IEntityPlayer.getPosY() + (double)yRange1, 0.0D, 256.0D);
                        int posZ = (int)(IEntityPlayer.getPosZ() + (double)zRange1);
                        IBlockPos pos = new IBlockPos((double)posX, (double)posY, (double)posZ);
                        IBlock block = IWorld.getBlockFromPos(pos);
                        if(block.getLocalizedName().toString().toLowerCase().contains("obsidian"))
                            blocks.add(block);
                    }
                }
            }
        }

        return blocks;
    }



    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {
        timer = 0;

//        updateBlocks();
    }

    @Override
    public void onDisable() {
        timer = 0;
    }
}
