package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import com.cdhoff.murdermystery.Client.Utils.Config;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.wrappers.IGameSettings;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

public class Fullbright extends Mod {

    public Fullbright() {
        super("Fullbright", "", ModType.FULLBRIGHT, GLFW.GLFW_KEY_G);
    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onDraw(Event event) {

    }

    @Override
    public void onWorld(Event event) {

    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {
        IMinecraft.setGamma(10);
    }

    @Override
    public void onDisable() {
        IMinecraft.setGamma(Config.fullbrightDefault);
    }
}
