package com.cdhoff.murdermystery.Client.Modules.Commands;

import com.cdhoff.murdermystery.Client.Client;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import com.cdhoff.murdermystery.Client.Utils.KeybindManager;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import me.deftware.client.framework.chat.ChatBuilder;
import me.deftware.client.framework.chat.style.ChatColors;
import me.deftware.client.framework.command.CommandBuilder;
import me.deftware.client.framework.command.CommandResult;
import me.deftware.client.framework.command.EMCModCommand;

public class CommandBind extends EMCModCommand {
    @Override
    public CommandBuilder getCommandBuilder() {
//        return new CommandBuilder().set(LiteralArgumentBuilder.literal("bind").executes(c -> {
//
//            IChat.sendClientMessage(getCommandBuilder().append(LiteralArgumentBuilder.literal("modname")).);
//            return 1;
//        }));

        /*return new CommandBuilder().set(LiteralArgumentBuilder.literal("bind")

                    .then(
                            RequiredArgumentBuilder.argument("mod", StringArgumentType.string())
                                    .then(RequiredArgumentBuilder.argument("bind",IntegerArgumentType.integer(-1,348)).executes(context ->

                                            {
                                                CommandResult r = new CommandResult(context);
                                                String modResult = r.getString("mod").trim();
                                                int bindResult = r.getInteger("bind");
                                                for(ModType modtype : ModType.values())
                                                {
                                                    if(modResult.equals(modtype.name().toLowerCase())){
                                                        Client.getInstance().getModloader().getMod(modResult).setKeybind(bindResult);
                                                        IChat.sendClientMessage(modResult.toUpperCase() + " keybind has been set to " + bindResult);
                                                    }
                                                }
                                                return 1;
                                            }
                                    ))
                    )
        );
    }*/
        return new CommandBuilder().set(LiteralArgumentBuilder.literal("bind")
            .then(RequiredArgumentBuilder.argument("mod", StringArgumentType.string()).executes(context -> {

                if(!KeybindManager.bind) {
                    CommandResult r = new CommandResult(context);
                    String modResult = r.getString("mod").trim();

                    KeybindManager.bind = true;
                    KeybindManager.latestModname = modResult;
                    new ChatBuilder().withText("Press key to set as binding for " + modResult).withColor(ChatColors.WHITE).build().print();
                }else{
                    new ChatBuilder().withText("You are already binding a key. Please complete the process and try again.").withColor(ChatColors.WHITE).build().print();
                }
                return 1;
            })
        ));
    }
}


/*
.executes(context -> {

                        CommandResult r = new CommandResult(context);

                        String modResult = r.getString("mod").trim();
                        int bindResult = r.getInteger("bind");

                        for(ModType modtype : ModType.values())
                        {
                            if(modResult.equals(modtype.name().toLowerCase())){
                                IChat.sendClientMessage(modResult + ", " + bindResult);
                            }
                        }
                        return 1;

                    }
 */
