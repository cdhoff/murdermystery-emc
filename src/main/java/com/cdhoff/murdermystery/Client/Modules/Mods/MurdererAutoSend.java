package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import org.lwjgl.glfw.GLFW;

public class MurdererAutoSend extends Mod {

    private static boolean autoSendStatus;

    public MurdererAutoSend() {
        super("MurderMysteryAutoSend", "Automatically send the names of the murderers in party chat.", ModType.MURDERMYSTERYAUTOSEND, GLFW.GLFW_KEY_PAGE_DOWN);
    }

    @Override
    public void onEvent(Event event) {
        if(!autoSendStatus){
            autoSendStatus = true;
        }
    }

    public static boolean getAutoSendStatus(){
        return autoSendStatus;
    }

    @Override
    public void onDraw(Event event) {

    }

    @Override
    public void onWorld(Event event) {

    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {
        autoSendStatus = true;
    }

    @Override
    public void onDisable() {
        autoSendStatus = false;
    }
}
