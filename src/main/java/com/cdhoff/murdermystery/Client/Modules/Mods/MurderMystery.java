package com.cdhoff.murdermystery.Client.Modules.Mods;


import com.cdhoff.murdermystery.Client.Client;
import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import com.cdhoff.murdermystery.Client.RenderManager;
import com.cdhoff.murdermystery.Client.Utils.Config;
import me.deftware.client.framework.chat.ChatBuilder;
import me.deftware.client.framework.chat.style.ChatColors;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.entity.IPlayer;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Optional;

public class MurderMystery extends Mod {

    public MurderMystery() {
        super("MurderMystery", "Tells you who the murderer is", ModType.MURDERMYSTERY, GLFW.GLFW_KEY_DELETE);
    }

    public static ArrayList<String> murderers = new ArrayList<String>();
    public String[] items = Config.items.split(",");
    public static String murderDisplay = "";

    public static DecimalFormat df = new DecimalFormat("0.0");


    @Override
    public void onEvent(Event event) {


        murderDisplay = "";

        ArrayList<IEntity> ents = new ArrayList<IEntity>();

        try {
            for (IEntity entity : IWorld.getLoadedEntities().values()) {


                //If the entity is a player
                if (entity.isPlayer()) {
                    ents.add(entity);

                    //Store the name of the player
                    String playerName = entity.getIPlayer().getName();
                    //Store the item the player is holding
                    String heldItem = entity.getIPlayer().getHeldItem().getIItem().getItemKey();
                    //For every item in the list of items
                    for (String item : items) {
                        //If the item the player is holding matches any of the items in the config
                        if (heldItem.matches(".*" + item + ".*")) {


                            if (!murderers.contains(playerName) && !Client.getSelf().getName().contains(playerName) && !entity.getIPlayer().getUUID().equals("07b88bfd-42ac-4e8c-8919-15d28dc9d162")) {

                                murderers.add(playerName);

                                //ISound sound = new PositionedSoundRecord(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1, (float)entity.posX, (float)entity.posY, (float)entity.posZ);

                                //Minecraft.getMinecraft().getSoundHandler().playSound(sound);
                                //Minecraft.getMinecraft().world.playSound(Minecraft.getMinecraft().player, Minecraft.getMinecraft().player.getPosition().getX(), Minecraft.getMinecraft().player.getPosition().getY(), Minecraft.getMinecraft().player.getPosition().getZ(),SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1.0f, 1.0f);



                                new ChatBuilder().withText(playerName + " is the murderer!").withColor(ChatColors.WHITE).build().print();


                                if (MurdererAutoSend.getAutoSendStatus())
                                    new ChatBuilder().withText(playerName + " is the murderer!").withColor(ChatColors.WHITE).build().sendMessage();

                            }

                        }
                    }
                }
            }

        } catch (NullPointerException e) {
        }

        for (String player : murderers) {
            for (IEntity ent : ents) {
                if (ent.getName().equals(player)) {
                    murderDisplay += player + " (" + df.format(ent.getDistanceToPlayer()) + ") | ";
                }
            }
        }

    }









//
//        murderDisplay = "";
//
//        ArrayList<IEntity> entities = new ArrayList<IEntity>();
//
//                IWorld.getLoadedEntities().stream().filter(e -> e.isPlayer()).forEach(e -> entities.add(e));
//
//            //For every loaded entity (Player, mob, etc.) in the world
//            for (IEntity entity : entities) {
//                //If the entity is a player
//                if (entity.isPlayer()) {
//                    //Store the name of the player
//                    String playerName = entity.getIPlayer().getName();
//                    //Store the item the player is holding
//                    String heldItem = entity.getIPlayer().getHeldItem().getIItem().getItemKey();
//
//
//
//                    //For every item in the list of items
//                    for (String item : items) {
//                        //If the item the player is holding matches any of the items in the config
//                        if (heldItem.matches(".*" + item + ".*")) {
//
//                            if (!murderers.contains(playerName)) {
//                                murderers.add(playerName);
//
//                                //Tells you that the player is the murderer
//                                IChat.sendClientMessage(playerName + " is the murderer!");
//                                //If the Murderer Auto Send mod is enabled
//                                if (MurdererAutoSend.getAutoSendStatus())
//                                    //Message every person in the party who the murderer is
//                                    IChat.sendChatMessage("/pchat " + playerName + " is the murderer!");
//                            }
//                        }
//                    }
//                }
//            }
//            for (String player : murderers) {
//                for (IEntity ent : entities) {
//                    if(ent.getName().equals(player)){
//                        if(!murderDisplay.contains(player))
//                            murderDisplay += player + " (" + df.format(ent.getDistanceToPlayer() + ") | ");
//                    }
//                }
//            }


    public static String formatString(String s){
        return Optional.ofNullable(s).filter(str -> s.length() != 0).map(str -> str.substring(0, str.length() - 2)).orElse(s);
    }


    @Override
    public void onDraw(Event event){
        if(!IMinecraft.isDebugInfoShown())
            RenderManager.drawMurderMystery();
    }

    @Override
    public void onWorld(Event event) {

    }

    @Override
    public void onNametag(EventNametagRender event) {

    }


    @Override
    public void onEnable() {
        murderers.clear();
        murderDisplay = "";
    }

    @Override
    public void onDisable() {
        murderers.clear();
        murderDisplay = "";
    }
}
