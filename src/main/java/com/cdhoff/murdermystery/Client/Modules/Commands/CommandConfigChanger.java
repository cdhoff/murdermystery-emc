package com.cdhoff.murdermystery.Client.Modules.Commands;

import com.cdhoff.murdermystery.Client.Utils.Config;
import com.mojang.brigadier.arguments.DoubleArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import me.deftware.client.framework.chat.ChatBuilder;
import me.deftware.client.framework.chat.style.ChatColors;
import me.deftware.client.framework.command.CommandBuilder;
import me.deftware.client.framework.command.CommandResult;
import me.deftware.client.framework.command.EMCModCommand;

public class CommandConfigChanger extends EMCModCommand {
    @Override
    public CommandBuilder<?> getCommandBuilder() {
        return new CommandBuilder().set(LiteralArgumentBuilder.literal("set")
                .then(LiteralArgumentBuilder.literal("espred")
                        .then(RequiredArgumentBuilder.argument("color", DoubleArgumentType.doubleArg())
                                .executes(context ->

                                    {
                                        CommandResult r = new CommandResult(context);

                                        Config.espRed = r.getDouble("color");
                                        Config.initialize();
                                        new ChatBuilder().withText("ESP Red value set to " + r.getDouble("color")).withColor(ChatColors.WHITE).build().print();
                                        return 1;
                                    }
                                )


                        )


                )
                .then(LiteralArgumentBuilder.literal("espgreen")
                        .then(RequiredArgumentBuilder.argument("color", DoubleArgumentType.doubleArg())
                                .executes(context ->

                                        {
                                            CommandResult r = new CommandResult(context);

                                            Config.espGreen = r.getDouble("color");
                                            Config.initialize();
                                            new ChatBuilder().withText("ESP Green value set to " + r.getDouble("color")).withColor(ChatColors.WHITE).build().print();
                                            return 1;
                                        }
                                )


                        )


                )
                .then(LiteralArgumentBuilder.literal("espblue")
                        .then(RequiredArgumentBuilder.argument("color", DoubleArgumentType.doubleArg())
                                .executes(context ->

                                        {
                                            CommandResult r = new CommandResult(context);

                                            Config.espBlue = r.getDouble("color");
                                            Config.initialize();
                                            new ChatBuilder().withText("ESP Blue value set to " + r.getDouble("color")).withColor(ChatColors.WHITE).build().print();
                                            return 1;
                                        }
                                )


                        )


                )
                .then(LiteralArgumentBuilder.literal("espopacity")
                        .then(RequiredArgumentBuilder.argument("opacity", DoubleArgumentType.doubleArg())
                                .executes(context ->

                                        {
                                            CommandResult r = new CommandResult(context);

                                            Config.espAlpha = r.getDouble("opacity");
                                            Config.initialize();
                                            new ChatBuilder().withText("ESP Opacity value set to " + r.getDouble("color")).withColor(ChatColors.WHITE).build().print();
                                            return 1;
                                        }
                                )


                        )


                )
                .then(LiteralArgumentBuilder.literal("espmobs")
                        .then(RequiredArgumentBuilder.argument("value", IntegerArgumentType.integer(0,1))
                                .executes(context ->

                                        {
                                            CommandResult r = new CommandResult(context);
                                            boolean temp = false;
                                            //Config.espAlpha = r.getDouble("opacity");
                                            switch(r.getInteger("value")){
                                                case 0:
                                                    temp = false;
                                                    break;
                                                case 1:
                                                    temp = true;
                                                    break;
                                                default:
                                                    temp = false;
                                            }

                                            Config.espMobs = temp;
                                            Config.initialize();
                                            new ChatBuilder().withText("ESP Mob value set to " + temp).withColor(ChatColors.WHITE).build().print();

                                            return 1;
                                        }
                                )


                        )


                ).then(LiteralArgumentBuilder.literal("espitems")
                        .then(RequiredArgumentBuilder.argument("value", IntegerArgumentType.integer(0,1))
                                .executes(context ->

                                        {
                                            CommandResult r = new CommandResult(context);
                                            boolean temp = false;
                                            //Config.espAlpha = r.getDouble("opacity");
                                            switch(r.getInteger("value")){
                                                case 0:
                                                    temp = false;
                                                    break;
                                                case 1:
                                                    temp = true;
                                                    break;
                                                default:
                                                    temp = false;
                                            }

                                            Config.espItems = temp;
                                            Config.initialize();
                                            new ChatBuilder().withText("ESP Items value set to " + temp).withColor(ChatColors.WHITE).build().print();
                                            return 1;
                                        }
                                )


                        )


                )




        );
    }
}
