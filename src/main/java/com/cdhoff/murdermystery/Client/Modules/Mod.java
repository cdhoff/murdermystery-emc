package com.cdhoff.murdermystery.Client.Modules;

import com.cdhoff.murdermystery.Client.Client;
import me.deftware.client.framework.chat.ChatBuilder;
import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.chat.ChatSection;
import me.deftware.client.framework.chat.style.ChatColors;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;

public abstract class Mod {

    private ModType type;
    private String name, description;
    private int keybind;
    private boolean state;
    private String string;

    /**
     * Set keybind to -1 for no keybind
     *
     * @param name
     * @param description
     * @param type
     * @param keybind
     */
    public Mod(String name, String description, ModType type, int keybind) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.keybind = Client.getSetting().getPrimitive(this.getID() + "_bind", keybind);
        this.state = Client.getSetting().getPrimitive(this.getID() + "_state", false);
    }

    /**
     * Saves state+keybind to config
     */
    private void save() {
        Client.getSetting().putPrimitive(this.getID() + "_bind", keybind);
        Client.getSetting().putPrimitive(this.getID() + "_state", state);
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public ModType getType() {
        return type;
    }

    public void setType(ModType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getKeybind() {
        return keybind;
    }

    public void setKeybind(int keybind) {
        this.keybind = keybind;
        save();
    }

    /**
     * Returns this mods unique ID
     */
    public String getID() {
        return this.name.toLowerCase().replace(" ", "_");
    }

    public void toggle() {
        this.state = !state;
        save();
        new ChatBuilder().withText(this.getName() + " is now " + (state ? "enabled" : "disabled")).withColor(ChatColors.WHITE).build().print();
        if (state) {
            this.onEnable();
        } else {
            this.onDisable();
        }
    }

    /*
     * Handlers
     */

    public abstract void onEvent(Event event);

    public abstract void onDraw(Event event);

    public abstract void onWorld(Event event);

    public abstract void onNametag(EventNametagRender event);

    public abstract void onEnable();

    public abstract void onDisable();


}
