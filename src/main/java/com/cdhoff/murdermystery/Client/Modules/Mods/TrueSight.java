package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;

public class TrueSight extends Mod {

    public TrueSight() {
        super("TrueSight", "", ModType.TRUESIGHT, GLFW.GLFW_KEY_PAGE_UP);
    }
    public static ArrayList<String> players = new ArrayList<String>();

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onDraw(Event event) {

    }


    @Override
    public void onWorld(Event event) {
        for (IEntity entity : IWorld.getLoadedEntities().values()) {
            if (entity.isInvisible()) {
                players.add(entity.getName());
                entity.getEntity().method_5648(false);
            }
        }
    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {
        for(IEntity entity : IWorld.getLoadedEntities().values()){
            if(players.contains(entity.getName()))
                entity.getEntity().method_5648(true);
        }
        players.clear();
    }
}
