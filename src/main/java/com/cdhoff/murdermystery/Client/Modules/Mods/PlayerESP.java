package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

public class PlayerESP extends Mod {

    public PlayerESP() {
        super("PlayerESP", "", ModType.PLAYERESP, GLFW.GLFW_KEY_INSERT);
    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onDraw(Event event) {

    }



    @Override
    public void onWorld(Event event) {
//        double red = Config.espRed;
//        double green = Config.espGreen;
//        double blue = Config.espBlue;
//        double alpha = Config.espAlpha;
        for(IEntity entity : IWorld.getLoadedEntities().values()){
            if(!entity.isSelf() && (entity.isPlayer() || entity.isPlayerOwned())) {
                if (MurderMystery.murderers.contains(entity.getName())) {
                    RenderUtils.ESPBox(entity.getBoundingBox(), 1, 0, 0, 0.2, 1, false);
                }else{
                    RenderUtils.ESPBox(entity.getBoundingBox(),0,0,1,0.2,1,false);
                }
            }
//                else if(entity.isInvisible() || TrueSight.players.contains(entity.getName())){
//                    RenderUtils.ESPBox(entity.getBoundingBox(),1,1,1,alpha,1,false);
//                }else{
//                    RenderUtils.ESPBox(entity.getBoundingBox(),red,green,blue,alpha,1,false);
//                }
//            if(entity.isItem() && Config.espItems)
//                RenderUtils.ESPBox(entity.getBoundingBox(),1,1,0,alpha,1,false);
//            if(entity.isMob() && Config.espMobs && !entity.isPlayer())
//                RenderUtils.ESPBox(entity.getBoundingBox(),1,0,1,alpha,1,false);

        }
    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }
}
