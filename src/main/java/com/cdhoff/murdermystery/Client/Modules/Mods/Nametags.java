package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.render.IFontRenderer;
import me.deftware.client.framework.wrappers.render.IGlStateManager;
import me.deftware.client.framework.wrappers.render.IRenderManager;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

import java.awt.*;
import java.text.DecimalFormat;

import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;

public class Nametags extends Mod {
    public Nametags() {
        super("Nametags", "", ModType.NAMETAGS, GLFW.GLFW_KEY_PAGE_UP);
    }
    public static DecimalFormat df = new DecimalFormat("0.0");



    @Override
    public void onEvent(Event event) {
    }

    @Override
    public void onDraw(Event event) {

    }



    @Override
    public void onWorld(Event event) {
        for(IEntity player : IWorld.getLoadedEntities().values()){
            if(player.isPlayer() && !player.isSelf()){
                double renderPosX = RenderUtils.getRenderManager().getRenderPosX();
                double renderPosY = RenderUtils.getRenderManager().getRenderPosY();
                double renderPosZ = RenderUtils.getRenderManager().getRenderPosZ();

                IGlStateManager.pushMatrix();
                IGlStateManager.translate( // Translate to player position with render pos and interpolate it
                        player.getLastTickPosX() + (player.getPosX() - player.getLastTickPosX()) * IMinecraft.getRenderPartialTicks() - renderPosX,
                        player.getLastTickPosY() + (player.getPosY() - player.getLastTickPosY()) * IMinecraft.getRenderPartialTicks() -  renderPosY + player.getEyeHeight() + 0.55,
                        player.getLastTickPosZ() + (player.getPosZ() - player.getLastTickPosZ()) * IMinecraft.getRenderPartialTicks() - renderPosZ
                );

                IGlStateManager.rotate(-IRenderManager.getPlayerViewY(), 0f, 1f, 0f);
                IGlStateManager.rotate(IRenderManager.getPlayerViewX(), 1f, 0f, 0f);

                float distance = player.getDistanceToEntity() / 4F;
                String text;

                //text = player.getFormattedDisplayName() + " " + Math.round(player.getHealth()) + "HP";

                if(MurderMystery.murderers.contains(player.getIPlayer().getName())) {
                    text = "\u00A7c" + player.getName() + " " + Math.round(player.getHealth()) + "HP " + df.format(player.getDistanceToPlayer());
                }else if(player.getHealth() < 6) {
                    text = player.getFormattedDisplayName() + " \u00A7c" + Math.round(player.getHealth()) + "HP \u00A7b" + df.format(player.getDistanceToPlayer());
                }else if(player.getHealth() < 15) {
                    text = player.getFormattedDisplayName() + " \u00A7e" + Math.round(player.getHealth()) + "HP \u00A7b" + df.format(player.getDistanceToPlayer());
                }else{
                    text = player.getFormattedDisplayName() + " \u00A7a" + Math.round(player.getHealth()) + "HP \u00A7b" + df.format(player.getDistanceToPlayer());
                }

                if(distance < 2F) distance = 2F;

                float scale = distance/100F * 1F;

                IGlStateManager.scale(-scale, -scale, scale);

                IGlStateManager.disableLighting();
                IGlStateManager.disableDepth();
                IGlStateManager.disableTexture2D();
                IGlStateManager.enableBlend();
                IGlStateManager.blendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                int width = IFontRenderer.getStringWidth(text) / 2;

                RenderUtils.drawRect(-width - 2f, -2f, width+4f, IFontRenderer.getFontHeight(), new Color(0,0,0,120));
                IFontRenderer.drawString(text, 1 + -width, 1, 0xFFFFFF);

                IGlStateManager.disableBlend();
                IGlStateManager.enableTexture2D();
                IGlStateManager.enableDepth();
                IGlStateManager.enableLighting();
                IGlStateManager.popMatrix();

            }
        }
    }

    @Override
    public void onNametag(EventNametagRender event) {
        if(event.isPlayer()){
            event.setCanceled(true);
        }
    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }
}
