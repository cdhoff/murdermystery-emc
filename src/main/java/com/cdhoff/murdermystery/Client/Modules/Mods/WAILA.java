package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.client.framework.wrappers.render.IFontRenderer;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

import java.awt.*;
import java.text.DecimalFormat;

public class WAILA extends Mod {

    public WAILA() {
        super("WAILA", "", ModType.WAILA, GLFW.GLFW_KEY_KP_ADD);
    }

    public static DecimalFormat df = new DecimalFormat("0.0");

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onDraw(Event event) {
        //System.out.println(IWorld.getBlockFromPos(IMinecraft.getBlockOver()).getLocalizedName());
        try{
            ChatMessage block = IWorld.getBlockFromPos(IMinecraft.getBlockOver()).getLocalizedName();
            if(!block.equals("Air"))
                IFontRenderer.drawCenteredString(block, IGuiScreen.getScaledWidth() / 2, 2, Color.white.getRGB());
        }catch(NullPointerException e){
            String entity = "";
            if(IMinecraft.getPointedEntity().getName() == null){
                entity = IMinecraft.getPointedEntity().getEntityTypeName();//
            }else{
                entity = IMinecraft.getPointedEntity().getName();
            }
            ChatMessage health = new ChatMessage().fromString(df.format(IMinecraft.getPointedEntity().getHealth()) + "/" + IMinecraft.getPointedEntity().getMaxHealth() + "HP");
            IFontRenderer.drawCenteredString(entity,IGuiScreen.getScaledWidth() / 2, 2, Color.white.getRGB());
            IFontRenderer.drawCenteredString(health,IGuiScreen.getScaledWidth() / 2, 2 + IFontRenderer.getFontHeight(), Color.white.getRGB());
        }
    }

    @Override
    public void onWorld(Event event) {

    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }
}
