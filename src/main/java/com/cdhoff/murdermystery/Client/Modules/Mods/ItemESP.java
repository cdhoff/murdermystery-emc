package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

public class ItemESP extends Mod {

    public ItemESP() {
        super("ItemESP", "", ModType.ITEMESP, GLFW.GLFW_KEY_KP_6);
    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onDraw(Event event) {

    }



    @Override
    public void onWorld(Event event) {
        for(IEntity entity : IWorld.getLoadedEntities().values()){
            if(entity.isItem())
                RenderUtils.ESPBox(entity.getBoundingBox(),1,1,0,0.2,1,false);
        }
    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }
}
