package com.cdhoff.murdermystery.Client.Modules.Mods;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;

public class ItemGlow extends Mod {

    public ItemGlow() {
        super("ItemGlow", "", ModType.ITEMGLOW, GLFW.GLFW_KEY_UNKNOWN);
    }

    @Override
    public void onEvent(Event event) {

        for(IEntity entity : IWorld.getLoadedEntities().values()) {
            if (entity.isItem()) {
                entity.getIItemEntity().setGlowing(true);
            }
        }
    }

    @Override
    public void onDraw(Event event) {

    }

    @Override
    public void onWorld(Event event) {

    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {
        for(IEntity entity : IWorld.getLoadedEntities().values()) {
                entity.getIItemEntity().setGlowing(false);
        }
    }
}
