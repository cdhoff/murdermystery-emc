package com.cdhoff.murdermystery.Client.Modules.Mods;


import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import com.cdhoff.murdermystery.Client.Utils.Config;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventNametagRender;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.glfw.GLFW;

public class Tracers extends Mod {
    public Tracers() {
        super("Tracers", "", ModType.TRACERS, GLFW.GLFW_KEY_END);
    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onDraw(Event event) {

    }

    @Override
    public void onWorld(Event event) {
        for(IEntity player : IWorld.getLoadedEntities().values()){
            if(player.isPlayer() && !player.isSelf()){
                if(MurderMystery.murderers.contains(player.getIPlayer().getName())) {
                    RenderUtils.tracerLine(player, 1);
                }else{
                    RenderUtils.tracerLine(player, 0);
                }
            }

        }
    }

    @Override
    public void onNametag(EventNametagRender event) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }
}
