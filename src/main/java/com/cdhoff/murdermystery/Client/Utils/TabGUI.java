package com.cdhoff.murdermystery.Client.Utils;

import com.cdhoff.murdermystery.Client.Client;
import com.cdhoff.murdermystery.Client.Modules.Mod;
import me.deftware.client.framework.event.events.EventKeyAction;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.render.IFontRenderer;
import org.lwjgl.glfw.GLFW;
import sun.reflect.generics.tree.Tree;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TabGUI {

    static Map<Integer, String> mods = new TreeMap<Integer,String>();
    static List<String> temp = new ArrayList<>();

    static int selectedMain = 0;

    public static void populateList(){
        for(Mod mod : Client.getInstance().getModloader().getModlist().values()){
            temp.add(mod.getName());
        }
        System.out.println("dick size: " + temp.size());
        int j = 0;
        for(int i = 0; i < temp.size();i++){
            mods.put(j,temp.get(j));
            j++;
        }
        System.out.println("dick " + mods);
    }

    public static void drawGui() {
        int i = 0;
        for (int j = 0; i < mods.size(); j++) {
            float height = ((float) IFontRenderer.getFontHeight() * i);
            //RenderUtils.drawBorderedRect(2f, height,IFontRenderer.getStringWidth("MurderMystery") + 2,height + IFontRenderer.getFontHeight(),1f,selected == i?0xcccccca0:0xffffff80,0xff000080);
            RenderUtils.drawRect(2f, height + 3, IFontRenderer.getStringWidth("MurderMystery") + 4, height + IFontRenderer.getFontHeight() + 3, selectedMain != i ? 0x80000000 : 0x80242424);
            IFontRenderer.drawString(mods.get(i), 4, (IFontRenderer.getFontHeight() * i) + 4, Client.getInstance().getModloader().getMod(mods.get(i)).isState() ? 0xFFFFFF : 0x7d7d7d);
            i++;
        }
        i=0;
        for (int j = 0; i < mods.size(); j++) {
            float height = ((float) IFontRenderer.getFontHeight() * i);
            RenderUtils.drawBorderedRect(2f, height + 3, IFontRenderer.getStringWidth("MurderMystery") + 4, height + IFontRenderer.getFontHeight() + 4, 1f, selectedMain != i ? 0x00000000 : 0xE6ffffff, 0x00000000);
            i++;
        }
    }

    public static void handleNavigation(EventKeyAction event) {
        if (event.getKeyCode() == GLFW.GLFW_KEY_DOWN) {
            if (selectedMain < mods.size() - 1) {
                selectedMain++;
            } else {
                selectedMain = 0;
            }
        } else if (event.getKeyCode() == GLFW.GLFW_KEY_UP) {
            if (selectedMain == 0) {
                selectedMain = mods.size() - 1;
            } else {
                selectedMain--;
            }
        } else if (event.getKeyCode() == GLFW.GLFW_KEY_RIGHT) {
            Client.getInstance().getModloader().getMod(mods.get(selectedMain)).toggle();
        }

    }


}
