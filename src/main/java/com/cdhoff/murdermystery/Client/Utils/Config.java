package com.cdhoff.murdermystery.Client.Utils;

import com.cdhoff.murdermystery.Client.Client;

public class Config {
    public static String items = Client.getSetting().getPrimitive("murdermystery_items", "sword,stick,hoe,axe,dead_bush,rod,feather,pie,apple,pick,name_tag,sponge,carrot,bone,double_plant,redstone_torch,fish");

    public static Double espRed = Client.getSetting().getPrimitive("esp_red", 0.0);
    public static Double espGreen = Client.getSetting().getPrimitive("esp_green", 0.0);
    public static Double espBlue = Client.getSetting().getPrimitive("esp_blue", 1.0);
    public static Double espAlpha = Client.getSetting().getPrimitive("esp_opacity", 0.2);
    public static Boolean espMobs = Client.getSetting().getPrimitive("esp_mobs", false);
    public static Boolean espItems = Client.getSetting().getPrimitive("esp_items", false);
    public static Double fullbrightDefault = Client.getSetting().getPrimitive("fullbright_default", 1.0);


    public static void initialize(){
        Client.getSetting().putPrimitive("murdermystery_items", items);
        Client.getSetting().putPrimitive("esp_red", espRed);
        Client.getSetting().putPrimitive("esp_green", espGreen);
        Client.getSetting().putPrimitive("esp_blue", espBlue);
        Client.getSetting().putPrimitive("esp_opacity", espAlpha);
        Client.getSetting().putPrimitive("esp_mobs", espMobs);
        Client.getSetting().putPrimitive("esp_items", espItems);
        Client.getSetting().putPrimitive("fullbright_default", fullbrightDefault);

    }
    
}
