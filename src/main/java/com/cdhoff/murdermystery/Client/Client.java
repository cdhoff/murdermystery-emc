package com.cdhoff.murdermystery.Client;

import com.cdhoff.murdermystery.Client.Modules.ModLoader;
import com.cdhoff.murdermystery.Main;
import me.deftware.client.framework.utils.Settings;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.entity.IPlayer;
import me.deftware.client.framework.wrappers.world.IWorld;

public class Client {

    private static Main main;
    private static Client instance;

    private ModLoader modloader = new ModLoader();

    public Client(Main main) {
        this.main = main;
        instance = this;
    }

    public static Settings getSetting() {
        return main.getSettings();
    }

    public void initialize() {
        modloader.initialize();
    }

    public ModLoader getModloader() {
        return modloader;
    }

    public static Client getInstance() {
        return instance;
    }

    public static IPlayer getSelf(){
        IPlayer player = null;
        for(IEntity entity : IWorld.getLoadedEntities().values()){
            if(entity.isSelf()){
                player = entity.getIPlayer();
                break;
            }
        }
        return player;
    }

}
