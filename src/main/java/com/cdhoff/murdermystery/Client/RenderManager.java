package com.cdhoff.murdermystery.Client;

import com.cdhoff.murdermystery.Client.Modules.Mod;
import com.cdhoff.murdermystery.Client.Modules.ModType;
import com.cdhoff.murdermystery.Client.Modules.Mods.MurderMystery;
import com.cdhoff.murdermystery.Client.Utils.KeyTranslator;
import com.cdhoff.murdermystery.Main;
import com.google.common.base.Functions;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.entity.IEntity;
import me.deftware.client.framework.wrappers.entity.IPlayer;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.client.framework.wrappers.gui.IScreens;
import me.deftware.client.framework.wrappers.render.IFontRenderer;
import me.deftware.client.framework.wrappers.render.IGlStateManager;
import me.deftware.client.framework.wrappers.render.IRenderHelper;
import me.deftware.client.framework.wrappers.render.IRenderManager;
import me.deftware.client.framework.wrappers.world.IWorld;
import org.lwjgl.system.CallbackI;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

public class RenderManager {

    public static Map<ModType,Mod> modlist = Client.getInstance().getModloader().getModlist();

    static Comparator c = new Comparator<String>() {
        public int compare(String s1, String s2) {
            return IFontRenderer.getStringWidth(s2) - IFontRenderer.getStringWidth(s1);
        }
    };

    public static void drawClientOverlay() {
        //Renders client name on top left
        IFontRenderer.drawStringWithShadow(Main.name, 2, 2, Color.WHITE.getRGB());

        //Renders each Mod that is enabled on top right
        int i = 0;

        //Map<ModType,Mod> sorted = new TreeMap<ModType, Mod>((Comparator<ModType>) (o1, o2) -> Integer.compare(modlist.get(o2).getName().length(), modlist.get(o1).getName().length()));

        //sorted.putAll(Client.getInstance().getModloader().getModlist());

        List<String> enabledMods = new ArrayList<String>();

        for(Mod m : Client.getInstance().getModloader().getModlist().values()){
            if(m.isState()){
                enabledMods.add(m.getName());
            }
        }

        Collections.sort(enabledMods,c);

        //enabledMods.sort(Comparator.comparingInt(String::length));



        for (String mod : enabledMods) {
            int offset = (IFontRenderer.getFontHeight() * i);

            IFontRenderer.drawStringWithShadow(mod,
                    IGuiScreen.getScaledWidth() - IFontRenderer.getStringWidth(mod) - 2,
                    2 + offset, Color.WHITE.getRGB());
            i += 1;
        }

    }

    public static void drawKeybinds(){
        int i = 0;

        Map<ModType,Mod> sorted = new TreeMap<ModType, Mod>((Comparator<ModType>) (o1, o2) -> Integer.compare(modlist.get(o2).getName().length(), modlist.get(o1).getName().length()));

        sorted.putAll(Client.getInstance().getModloader().getModlist());

        for(Mod m : Client.getInstance().getModloader().getModlist().values()){
            int offset = (IFontRenderer.getFontHeight() * i);
            String temp = m.getName() + ": " + KeyTranslator.keys.get(m.getKeybind());
            IFontRenderer.drawStringWithShadow(temp, IGuiScreen.getScaledWidth() - IFontRenderer.getStringWidth(temp),IGuiScreen.getScaledHeight() - IFontRenderer.getFontHeight() - offset, Color.WHITE.getRGB());
            i += 1;
        }
    }

    public static void drawMurderMystery() {
        //IFontRenderer.drawStringWithShadow("Murderer: " + MurderMystery.formatString(MurderMystery.murderDisplay), 2, IFontRenderer.getFontHeight() + 2, Color.WHITE.getRGB());
        IFontRenderer.drawStringWithShadow("Murderer: " + MurderMystery.formatString(MurderMystery.murderDisplay), 2, IGuiScreen.getScaledHeight() - IFontRenderer.getFontHeight(), Color.WHITE.getRGB());
    }

    public static void drawCoords() {
        String coords = "";
        for (IEntity entity : IWorld.getLoadedEntities().values()) {
            if (entity.isSelf()) {
                IPlayer player = entity.getIPlayer();
                coords = "XYZ: " + player.getBlockPos().toCords();
            }
        }
        if (Client.getInstance().getModloader().getMod(ModType.MURDERMYSTERY).isState()) {
            IFontRenderer.drawStringWithShadow(coords, 2, IGuiScreen.getScaledHeight() - IFontRenderer.getFontHeight() - 2 - IFontRenderer.getFontHeight(), Color.white.getRGB());
        }else{
            IFontRenderer.drawStringWithShadow(coords, 2, IGuiScreen.getScaledHeight() - IFontRenderer.getFontHeight(), Color.white.getRGB());
        }
    }
}