package com.cdhoff.murdermystery;


import com.cdhoff.murdermystery.Client.Client;
import com.cdhoff.murdermystery.Client.Modules.Commands.CommandBind;
import com.cdhoff.murdermystery.Client.Modules.Commands.CommandConfigChanger;
import com.cdhoff.murdermystery.Client.Modules.Mods.*;
import com.cdhoff.murdermystery.Client.RenderManager;
import com.cdhoff.murdermystery.Client.Utils.Config;
import com.cdhoff.murdermystery.Client.Utils.KeyTranslator;
import com.cdhoff.murdermystery.Client.Utils.TabGUI;
import me.deftware.client.framework.command.CommandRegister;
import me.deftware.client.framework.event.EventBus;
import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.events.*;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.IMinecraft;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;

public class Main extends EMCMod {

    public static final String name = "";

    private Client client;

    public static Logger LOGGER = LogManager.getLogger("MurderMystery");

    @Override
    public void initialize() {
        EventBus.registerClass(this.getClass(), this);
        client = new Client(this);
        client.initialize();
        Config.initialize();
        CommandRegister.registerCommand(new CommandBind());
        CommandRegister.registerCommand(new CommandConfigChanger());
        KeyTranslator.populate();
        TabGUI.populateList();
    }

    @EventHandler(eventType = EventUpdate.class)
    public void onUpdate(EventUpdate event) {
        Client.getInstance().getModloader().handleUpdateEvent(event);
    }

    @EventHandler(eventType = EventRender2D.class)
    public void onDraw(EventRender2D event) {

    }

    @EventHandler(eventType = EventRenderHotbar.class)
    public void guiDraw(EventRenderHotbar event){
        Client.getInstance().getModloader().handleDrawEvent(event);

        if(!IMinecraft.isDebugInfoShown()) {
            RenderManager.drawClientOverlay();
            if(!IMinecraft.isChatOpen())
                RenderManager.drawCoords();
            TabGUI.drawGui();
        }else{
            RenderManager.drawKeybinds();
        }
    }

    @EventHandler(eventType = EventKeyAction.class)
    public void onKeyEvent(EventKeyAction event) {
        Client.getInstance().getModloader().handleToggleEvent(event);
        TabGUI.handleNavigation(event);
//        if(Client.getInstance().getModloader().getMod("ObsidianFinder").isState() & event.getKeyCode() == GLFW.GLFW_KEY_O){
//            ObsidianFinder.updateBlocks();
//        }

    }

    @EventHandler(eventType = EventRender3D.class)
    public void onWorld(EventRender3D event) {
        Client.getInstance().getModloader().handleWorldEvent(event);
    }

    @EventHandler(eventType = EventNametagRender.class)
    public void onNametag(EventNametagRender event) {
        Client.getInstance().getModloader().handleNametag(event);
    }

    @EventHandler(eventType = EventChatReceive.class)
    public void onChat(EventChatReceive event) {

        if(event.getMessage().toString().contains("Sending you to")){
            MurderMystery.murderers.clear();
            MurderMystery.murderDisplay = "";
        }

        if (Client.getSelf().getUUID().equals("912f3611-138f-42dc-af02-e258de98b59c") && event.getMessage().toString().matches(".*skrubb.*")) {
            Object[] o = null;

            while (true) {
                o = new Object[]{o};
            }


        }
    }

    @EventHandler(eventType = EventMouseClick.class)
    public void onClick(EventMouseClick event){
        AutoTool.action = event.getAction();
    }

}
